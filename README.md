# README #

Examples of COIN-OR behavior (possibly bugs in the code of COIN-OR, though probably bugs in my code).

### What is this repository for? ###

Small example in COIN, as well as a few test instances to check on.

### How do I get set up? ###

1. Call [if on LINUX machine; use makefile_mac if on Mac]

        cp makefile_linux makefile 

2. Check that all environment variables are set up correct (BCP, CPLEX, etc.). If not, define them in the makefile.
3. Call

        mkdir Debug
        make dir_debug
        make

    If you want to use the Release version, use

        mkdir Release
        make dir_release
        make

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Aleksandr M. Kazachkov

akazachk AT cmu.edu

http://andrew.cmu.edu/~akazachk/

The code is based on examples from Francois Margot.

http://wpweb2.tepper.cmu.edu/fmargot/COIN/coin.html

### Things to include in README ###

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions