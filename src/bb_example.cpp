/**
 * @file bb_example.cpp
 * @author A. M. Kazachkov
 * @date 2020-08-10
 */
/* Purpose: testing various COIN-OR methods and perhaps bugs */

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cfloat>
#include <limits>

#include <sys/stat.h> // For checking whether a file exists

#include <CbcBranchDefaultDecision.hpp>
#include <CbcCompareObjective.hpp>
#include <CbcEventHandler.hpp>
#include <CbcModel.hpp>
#include <CbcTree.hpp>
#include <CglGMI.hpp>
#include <OsiClpSolverInterface.hpp>

const double EPS = 1e-7;

/// An event handler that records the time that a better solution is found
class CustomHandler : public CbcEventHandler {
 public:
  CustomHandler() {};

  virtual CbcEventHandler* clone() const {
    return new CustomHandler(*this);
  }

  virtual CbcAction event(CbcEventHandler::CbcEvent whichEvent) {
    if (whichEvent == CbcEventHandler::treeStatus) {
      printf("\n## Number nodes: %d. Current depth: %d. ##\n", model_->getNodeCount2(), model_->currentDepth());
      /*
      const int numNodesOnTree_ = model_->tree()->size();
      int numLeafNodes_ = 0;
      for (int i = 0; i < numNodesOnTree_; i++) {
        numLeafNodes_ += model_->tree()->nodePointer(i)->nodeInfo()->numberBranchesLeft();
      }
      printf("\n## Number nodes on tree: %d. Number leaf nodes: %d. Depth: %d. Node count: %d. ##\n",
          numNodesOnTree_, numLeafNodes_, model_->currentDepth(), model_->getNodeCount2());
      */
    }
    return CbcEventHandler::noAction;
  }
};

/**********************************************************/
/**
 * Check if a file exists
 */
bool fexists(const char* filename) {
  struct stat buffer;
  return (stat (filename, &buffer) == 0);
}

/**********************************************************/
int main(int argc, char** argv) 
{
  std::string dir, outdir, f_name, f_name_stub, prob_name;
  std::string tmppath, instdir, tmpdir, tmpname;

  if (argc < 2) {
    printf("Name of the input file (.lp or .mps):\n");
    std::getline(std::cin, tmppath);
  } else {
    tmppath = argv[1];
  }

  // Extract the overall directory as well as the instance directory and filename stub
  size_t found_slash = tmppath.find_last_of("/\\");
  if ((found_slash != std::string::npos) && (found_slash < tmppath.length())) {
    instdir = tmppath.substr(0, found_slash + 1);
    tmpname = tmppath.substr(found_slash + 1);

    // We can now look for the second slash, to see where to save the instances information
    size_t found_slash2 = instdir.find_last_of("/\\",
        instdir.length() - 2);
    if ((found_slash2 != std::string::npos) && (found_slash2 < instdir.length())
        && (found_slash2 != instdir.length() - 1)) {
      tmpdir = instdir.substr(0, found_slash2 + 1);
    } else
      // Otherwise, only one folder was specified, so the current directory is where to save instance info
      tmpdir = "./";
  } else { // Otherwise, no directory was specified, so the parent directory is where to save instance info
    tmpname = tmppath;
    instdir = "./";
    tmpdir = "../";
  }

  // Check to make sure that tmpdir has a '/' on the end
  if (!tmpdir.empty() && tmpdir.at(tmpdir.size() - 1) != '/')
    tmpdir += '/';

  // Check to make sure that instdir has a '/' on the end
  if (!instdir.empty() && instdir.at(instdir.size() - 1) != '/')
    instdir += '/';

  printf(
      "\nThe overall directory is set to \"%s\".\nThe instance directory is \"%s\".\nThe filename is \"%s\".\n",
      tmpdir.c_str(), instdir.c_str(), tmpname.c_str());

  const std::string TEST_DIR = tmpdir;
  const std::string TEST_NAME = tmpname;
  // Get file name stub
  unsigned found_dot = TEST_NAME.find_last_of('.');

  // Put string after last '.' into string in_file_ext
  if (found_dot >= TEST_NAME.length()) {
    std::cerr
      << "*** ERROR: Cannot find the file extension (no '.' in input file name).\n"
      << std::endl;
    exit(1);
  }

  prob_name = TEST_NAME.substr(0, found_dot);

  std::string tmp_file_ext(TEST_NAME.substr(found_dot + 1));
  unsigned found_dot_tmp = found_dot;

  if (tmp_file_ext.compare("gz") == 0) {
    found_dot_tmp = prob_name.find_last_of('.');

    // Put string after last '.' into string in_file_ext
    if (found_dot_tmp >= prob_name.length()) {
      std::cerr
        << "*** ERROR: Other than gz, cannot find the file extension (no '.' in input file name).\n"
        << std::endl;
      exit(1);
    }

    tmp_file_ext = prob_name.substr(found_dot_tmp + 1);
    prob_name = prob_name.substr(0, found_dot_tmp);
  }

  const std::string in_file_ext(tmp_file_ext);
  dir.assign(instdir);

#ifdef TRACE
  std::cout << "\nProblem name is \"" + prob_name + "\"." << std::endl;
  std::cout << "\nExtension is \"" + in_file_ext + "\"." << std::endl;
  std::cout << "\nDirectory for instance has been set to \"" + dir + "\"."
    << std::endl;
#endif

  // Set f_name to directory+TEST_NAME. Same for the stub.
  f_name = dir + TEST_NAME;
  f_name_stub = dir + prob_name;

  // Check that the file exists
  bool f_exists = fexists(f_name.c_str());
  if (!f_exists) {
    // Check if maybe it is a compressed file and the ".gz" extension was forgotten
    f_exists = fexists((f_name + ".gz").c_str());
    if (f_exists) {
#ifdef TRACE
      std::cout << "Inputted filename was missing .gz extension. "
        << "As no such file exists, but an archived version does exist, working with the archived file."
        << std::endl;
#endif
      f_name = f_name + ".gz";
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
        "*** ERROR: File %s does not exist!\n", f_name.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }   
  }

  OsiClpSolverInterface* solver = new OsiClpSolverInterface;

  if (in_file_ext.compare("lp") == 0) {
    // Read LP file
#ifdef TRACE
    std::cout << "Reading LP file." << std::endl;
#endif
    solver->readLp(f_name.c_str());
  } else {
    if (in_file_ext.compare("mps") == 0) {
      // Read MPS file
#ifdef TRACE
      std::cout << "Reading MPS file." << std::endl;
#endif
      solver->readMps(f_name.c_str());
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Unrecognized extension: %s.\n",
          in_file_ext.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }
  }

  solver->initialSolve();
  fprintf(stdout, "Initial LP value: %.6f\n", solver->getObjValue());


  printf("\n## Starting branch-and-bound ##\n");
  CbcModel* cbc_model = new CbcModel;
  cbc_model->swapSolver(solver);
  cbc_model->setModelOwnsSolver(true); // solver will be deleted with cbc object

  // Set Cbc settings
  const int max_time = 120;
  const int num_strong = 5; //std::numeric_limits<int>::max();
  const int num_before_trusted = std::numeric_limits<int>::max(); // 10;
  cbc_model->setPrintFrequency(0); // to reach tree status correctly in latest Cbc
  cbc_model->setSecsPrintFrequency(-1); // to reach tree status correctly in latest Cbc
  cbc_model->solver()->setIntParam(OsiMaxNumIterationHotStart, 100);
  cbc_model->setTypePresolve(0);
  cbc_model->setMaximumSeconds(max_time);
  cbc_model->setMaximumCutPassesAtRoot(0);
  cbc_model->setMaximumCutPasses(0);
  cbc_model->setWhenCuts(0);
  if (num_strong >= 0) {
    // Maximum number of strong branching candidates to consider each time
    cbc_model->setNumberStrong(num_strong);
  }
  if (num_before_trusted >= 0) {
    // # before switching to pseudocosts, I think; 0 disables dynamic strong branching, doesn't work well
    cbc_model->setNumberBeforeTrust(num_before_trusted);
  }

  /*
  CbcBranchDecision* branch = NULL; //new CbcBranchDefaultDecision();
  if (branch) { cbc_model->setBranchingMethod(*branch); }

  CbcCompareBase* compare = new CbcCompareObjective();
  if (compare) { cbc_model->setNodeComparison(compare); }

  // Free memory for setting Cbc parameters
  if (branch) { delete branch; }
  if (compare) { delete compare; }
  */

  CustomHandler* eventHandler = new CustomHandler;
  cbc_model->passInEventHandler(eventHandler);

  cbc_model->setSecsPrintFrequency(-1.); // to reach tree status correctly in latest Cbc
  printf("secs print freq: %f\n", cbc_model->secsPrintFrequency());

  // Perform branch-and-bound
  cbc_model->branchAndBound(3);
  printf("secs print freq: %f\n", cbc_model->secsPrintFrequency());

  // Free memory
  if (eventHandler) { delete eventHandler; }
  if (cbc_model) { delete cbc_model; }
  //if (solver) { delete solver; }
} /* main */
