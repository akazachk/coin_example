/**
 * @file main.cpp
 * @author A. M. Kazachkov
 * @date 2019-11-21
 */
/* Purpose: testing various COIN-OR methods and perhaps bugs */

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cfloat>

#include <sys/stat.h> // For checking whether a file exists

#include <OsiSolverInterface.hpp>
#include <OsiClpSolverInterface.hpp>

#include <CglGMI.hpp>

const double EPS = 1e-7;
const int NUMSPACES = 10;

/**********************************************************/
/**
 * Check if a file exists
 */
bool fexists(const char* filename) {
  struct stat buffer;
  return (stat (filename, &buffer) == 0);
}

/**********************************************************/
int main(int argc, char** argv) 
{
  std::string dir, outdir, f_name, f_name_stub, prob_name;
  std::string tmppath, instdir, tmpdir, tmpname;

  if (argc < 2) {
    printf("Name of the input file (.lp or .mps):\n");
    std::getline(std::cin, tmppath);
  } else {
    tmppath = argv[1];
  }

  // Extract the overall directory as well as the instance directory and filename stub
  size_t found_slash = tmppath.find_last_of("/\\");
  if ((found_slash != std::string::npos) && (found_slash < tmppath.length())) {
    instdir = tmppath.substr(0, found_slash + 1);
    tmpname = tmppath.substr(found_slash + 1);

    // We can now look for the second slash, to see where to save the instances information
    size_t found_slash2 = instdir.find_last_of("/\\",
        instdir.length() - 2);
    if ((found_slash2 != std::string::npos) && (found_slash2 < instdir.length())
        && (found_slash2 != instdir.length() - 1)) {
      tmpdir = instdir.substr(0, found_slash2 + 1);
    } else
      // Otherwise, only one folder was specified, so the current directory is where to save instance info
      tmpdir = "./";
  } else { // Otherwise, no directory was specified, so the parent directory is where to save instance info
    tmpname = tmppath;
    instdir = "./";
    tmpdir = "../";
  }

  // Check to make sure that tmpdir has a '/' on the end
  if (!tmpdir.empty() && tmpdir.at(tmpdir.size() - 1) != '/')
    tmpdir += '/';

  // Check to make sure that instdir has a '/' on the end
  if (!instdir.empty() && instdir.at(instdir.size() - 1) != '/')
    instdir += '/';

  printf(
      "\nThe overall directory is set to \"%s\".\nThe instance directory is \"%s\".\nThe filename is \"%s\".\n",
      tmpdir.c_str(), instdir.c_str(), tmpname.c_str());

  const std::string TEST_DIR = tmpdir;
  const std::string TEST_NAME = tmpname;
  // Get file name stub
  unsigned found_dot = TEST_NAME.find_last_of('.');

  // Put string after last '.' into string in_file_ext
  if (found_dot >= TEST_NAME.length()) {
    std::cerr
      << "*** ERROR: Cannot find the file extension (no '.' in input file name).\n"
      << std::endl;
    exit(1);
  }

  prob_name = TEST_NAME.substr(0, found_dot);

  std::string tmp_file_ext(TEST_NAME.substr(found_dot + 1));
  unsigned found_dot_tmp = found_dot;

  if (tmp_file_ext.compare("gz") == 0) {
    found_dot_tmp = prob_name.find_last_of('.');

    // Put string after last '.' into string in_file_ext
    if (found_dot_tmp >= prob_name.length()) {
      std::cerr
        << "*** ERROR: Other than gz, cannot find the file extension (no '.' in input file name).\n"
        << std::endl;
      exit(1);
    }

    tmp_file_ext = prob_name.substr(found_dot_tmp + 1);
    prob_name = prob_name.substr(0, found_dot_tmp);
  }

  const std::string in_file_ext(tmp_file_ext);
  dir.assign(instdir);

#ifdef TRACE
  std::cout << "\nProblem name is \"" + prob_name + "\"." << std::endl;
  std::cout << "\nExtension is \"" + in_file_ext + "\"." << std::endl;
  std::cout << "\nDirectory for instance has been set to \"" + dir + "\"."
    << std::endl;
#endif

  // Set f_name to directory+TEST_NAME. Same for the stub.
  f_name = dir + TEST_NAME;
  f_name_stub = dir + prob_name;

  // Check that the file exists
  bool f_exists = fexists(f_name.c_str());
  if (!f_exists) {
    // Check if maybe it is a compressed file and the ".gz" extension was forgotten
    f_exists = fexists((f_name + ".gz").c_str());
    if (f_exists) {
#ifdef TRACE
      std::cout << "Inputted filename was missing .gz extension. "
        << "As no such file exists, but an archived version does exist, working with the archived file."
        << std::endl;
#endif
      f_name = f_name + ".gz";
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
        "*** ERROR: File %s does not exist!\n", f_name.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }   
  }

  OsiClpSolverInterface* solver = new OsiClpSolverInterface;

  if (in_file_ext.compare("lp") == 0) {
    // Read LP file
#ifdef TRACE
    std::cout << "Reading LP file." << std::endl;
#endif
    solver->readLp(f_name.c_str());
  } else {
    if (in_file_ext.compare("mps") == 0) {
      // Read MPS file
#ifdef TRACE
      std::cout << "Reading MPS file." << std::endl;
#endif
      solver->readMps(f_name.c_str());
    } else {
      char errorstring[256];
      snprintf(errorstring, sizeof(errorstring) / sizeof(char),
          "*** ERROR: Unrecognized extension: %s.\n",
          in_file_ext.c_str());
      std::cerr << errorstring << std::endl;
      exit(1);
    }
  }

  const double* obj = solver->getObjCoefficients();
  const double* zeroObj = new double[solver->getNumCols()];
  solver->setObjective(zeroObj);
  solver->writeMps("seymour1_presolved_prlp_zero","mps",1);

  solver->initialSolve();
  fprintf(stdout, "Initial LP value: %.6f\n", solver->getObjValue());
  exit(1);


  solver->enableFactorization();
  std::vector<int> varBasicInRow(solver->getNumRows(), -1);
  solver->getBasics(&varBasicInRow[0]);
  solver->disableFactorization();
  /*
  OsiCuts gmics;
  CglGMI GMIGen;
  GMIGen.getParam().setMAX_SUPPORT(solver->getNumCols());
  GMIGen.getParam().setMAX_SUPPORT_REL(0.5);
  GMIGen.generateCuts(*solver, gmics);
  solver->applyCuts(gmics);
  solver->resolve();
  fprintf(stdout, "After cuts LP value: %.6f\n", solver->getObjValue());
  */
} /* main */
