#!/usr/bin/env bash

COIN_OR=${REPOS_DIR}/coin-or/Cbc-trunk/buildg
COIN_OR=${REPOS_DIR}/vpc/lib/Cbc-trunk/buildg

g++ test_seymour.cpp -Wl,-rpath ${COIN_OR}/lib -isystem ${COIN_OR}/include/coin-or -o test_seymour -L${COIN_OR}/lib -lCbcSolver -lCbc -lOsiClp -lCgl -lOsi -lClp -lCoinUtils
