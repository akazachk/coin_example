* ENCODING=ISO-8859-1
NAME          stein09_nosym.lp
ROWS
 N  OBJ     
 G  A1      
 G  A2      
 G  A3      
 G  A4      
 G  A5      
 G  A6      
 G  A7      
 G  A8      
 G  A9      
 G  A10     
 G  A11     
 G  A12     
COLUMNS
    MARK0000  'MARKER'                 'INTORG'
    x1        OBJ                             1
    x1        A2                              1
    x1        A3                              1
    x1        A7                              1
    x1        A10                             1
    x2        OBJ                             2
    x2        A1                              1
    x2        A3                              1
    x2        A8                              1
    x2        A11                             1
    x3        OBJ                             3
    x3        A1                              1
    x3        A2                              1
    x3        A9                              1
    x3        A12                             1
    x4        OBJ                             4
    x4        A1                              1
    x4        A5                              1
    x4        A6                              1
    x4        A10                             1
    x5        OBJ                             5
    x5        A2                              1
    x5        A4                              1
    x5        A6                              1
    x5        A11                             1
    x6        OBJ                             6
    x6        A3                              1
    x6        A4                              1
    x6        A5                              1
    x6        A12                             1
    x7        OBJ                             7
    x7        A4                              1
    x7        A8                              1
    x7        A9                              1
    x7        A10                             1
    x8        OBJ                             8
    x8        A5                              1
    x8        A7                              1
    x8        A9                              1
    x8        A11                             1
    x9        OBJ                             9
    x9        A6                              1
    x9        A7                              1
    x9        A8                              1
    x9        A12                             1
    MARK0001  'MARKER'                 'INTEND'
RHS
    rhs       A1                              1
    rhs       A2                              1
    rhs       A3                              1
    rhs       A4                              1
    rhs       A5                              1
    rhs       A6                              1
    rhs       A7                              1
    rhs       A8                              1
    rhs       A9                              1
    rhs       A10                             1
    rhs       A11                             1
    rhs       A12                             1
BOUNDS
 UP bnd       x1                              1
 UP bnd       x2                              1
 UP bnd       x3                              1
 UP bnd       x4                              1
 UP bnd       x5                              1
 UP bnd       x6                              1
 UP bnd       x7                              1
 UP bnd       x8                              1
 UP bnd       x9                              1
ENDATA
