#include <OsiClpSolverInterface.hpp>
#include <string>
#include <vector>
#include <cstdio>

int main(int argc, char** argv) {
  OsiClpSolverInterface* solver = new OsiClpSolverInterface;

  std::string filename = "seymour1_presolved_prlp.mps.gz";
  if (argc > 1)
    filename = argv[1];

  if (filename.substr(filename.find_last_of(".")).compare(".lp") == 0) {
    solver->readLp(filename.c_str());
  } else {
    solver->readMps(filename.c_str());
  }

  printf("\nSolving with initial obj.\n");
  solver->initialSolve();
  printf("Initial solved value: %f\n", solver->getObjValue());

  std::vector<double> obj(solver->getObjCoefficients(), solver->getObjCoefficients() + solver->getNumCols());
  for (int i = 0; i < solver->getNumCols(); i++) {
    solver->setObjCoeff(i, 0.);
  }
  printf("\nSolving with zero obj.\n");
  solver->initialSolve();
  for (int i = 0; i < solver->getNumCols(); i++) {
    solver->setObjCoeff(i, 1.);
  }
  printf("\nResolving with all-ones obj.\n");
  solver->resolve();
  for (int i = 0; i < solver->getNumCols(); i++) {
    solver->setObjCoeff(i, obj[i]);
  }
  printf("\nResolving with original obj.\n");
  solver->resolve();
  printf("Resolved value: %f\n", solver->getObjValue());

  printf("\nAnother initial solve with original obj.\n");
  solver->initialSolve();
  printf("Initial solved value: %f\n", solver->getObjValue());

  if (solver) { delete solver; }
  return 0;
}
